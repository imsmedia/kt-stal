module.exports = function(grunt) {

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist: {
                src: [
                    'public/evth/js/libs/jquery.js',
                    'public/evth/js/libs/underscore.js',
                    'public/evth/js/libs/backbone.js',
                    'public/evth/js/common.js',
                    'public/evth/js/script.js',
                ],
                dest: 'public/evth/js/build/global.js'
            }
        },
        uglify: {
            build: {
                src: 'public/evth/js/build/global.js',
                dest: 'public/evth/js/build/global.min.js',
            }
        },
        imagemin: {
            dinamic: {
                files: [{
                    expand: true,
                    cwd: 'img/src/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'img/build/'
                }]
            }
        },
        watch: {
            scripts: {
                files: ['public/evth/js/*.js','resources/views/*.php','resources/views/**/*.php'],
                // tasks: ['concat', 'uglify'],
                tasks: [],
                options: {
                    spawn: false,
                    livereload: true,
                },
            },
            css: {
                files: ['public/evth/scss/*.scss'],
                tasks: ['sass'],
                options: {
                    spawn: false,
                    livereload: true,
                },
            },
        },
        sass: {
            dist: {
                options: {
                    compass: true,
                    style: 'compressed'
                },
                files: {
                    'public/evth/css/style.css': 'public/evth/scss/style.scss',
                    'public/evth/css/price.css': 'public/evth/scss/price.scss',
                    'public/evth/css/auth.css': 'public/evth/scss/auth.scss'
                }
            }
        },
        compass: {
            dist: {
                options: {
                    sassDir: 'public/evth/scss',
                    cssDir: 'public/evth/css',
                    environment: 'production',
                    sourcemap: true,
                    outputStyle: 'compressed'
                }
            },
        }

    });

    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
    // grunt.loadNpmTasks('grunt-contrib-concat');
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    // grunt.loadNpmTasks('grunt-contrib-imagemin');
    // grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-contrib-sass');
    // grunt.loadNpmTasks('grunt-contrib-compass');
    require('load-grunt-tasks')(grunt);
    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
    grunt.registerTask('default', ['watch']);

};
