<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Evth\Models\Product;
use Evth\Models\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PublicController extends Controller
{
    public function __construct(Product $product, Category $category){
      $this->product = $product;
      $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function showPrice()
    {
      $products = $this->product->all();
      $categories = $this->category->all();
      $cats=[];
      foreach ($categories as $key => $value) {
        $categories[$key]->childs;
        // if(count($categories[$key]->products()->get()->toArray())===0) {
        //   $categories->forget($key);
        // } else {
        //   $cats[] = $categories[$key];
        // }
         $cats[] = $categories[$key];
      }
      $arr = [
        'products'=>$products,
        'categories'=>$cats
      ];
      return view('price')->withArr($arr);
    }
    public function showDataJson()
    {
      // $category = $this->category->where('name', [
      //   'Арматура'
      // ])->first();
      // if ($category->childs->count()>0) {
      //
      // }
      //
      //
      // return $category;

      $products = $this->product->all();
      $categories = $this->category->all();
      foreach ($categories as $category) {
        $category->childs;
      }
      // $cats=[];
      // foreach ($categories as $key => $value) {
      //   if(count($categories[$key]->products()->get()->toArray())===0) {
      //     $categories->forget($key);
      //   } else {
      //     $cats[] = $categories[$key];
      //   }
      // }
      $arr = [
        'products'=>$products,
        'categories'=>$categories
      ];
      return 'data='.json_encode($arr);
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
