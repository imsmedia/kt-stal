App.Models.Category = Backbone.Model.extend();
//Модель модального окна загрузки xml
App.Models.XMLModal = Backbone.Model.extend({
  defaults: {
    header: 'Загрузить XML файл',
    file: undefined,
  },
  urlRoot: '/xml',
  initialize: function() {
    this.on('invalid', this.showError, this);
    this.on('sync', this.doResponse, this);
  },
  validate: function(attrs) {
    if (attrs.file === undefined) return 'Вы не выбрали файл';
    var types = ['text/xml', 'application/xml'];
    if ( _.indexOf(types, attrs.file.type)===-1 ) {
      return 'Неверный тип файла';
    };
  },
  save: function(options) {
    if (!this.isValid()) return;
    var file = this.get('file'),
      formData = new FormData();
    formData.append('file', file, file.name);
    $.ajax({
        url: '/xml',
        type: 'POST',
        dataType: 'text',
        processData: false,
        contentType: false,
        data: formData,
      })
      .done(function(resp) {
        var response = JSON.parse(resp);
        options.success.call(this, response);
      })
      .fail(function(resp) {
        var response;
        try {
          response = JSON.parse(resp);
        } catch (e) {
          response = {
            status: 'error',
            message: 'Что-то разлетелось на кусочки'
          }
        } finally {
          options.error.call(this, response);
        }
      })
      .always(function() {
        options.always.call(this);
      });
  },
  showError: function(model, error) {
    log(error);
  },
  doResponse: function(model, response) {
    if (response.status == 'ok') {
      this.showError(this, 'Загружено');
      vent.trigger('XMLLoadedSuccess');
    } else {
      this.showError(this, 'Ошибка, проверьте правильность ввода');
      vent.trigger('XMLLoadedError');
    };
  },
});
//Вид модального окна загрузки xml
App.Views.XMLModal = Backbone.View.extend({
  template: _.template($('#xml-modal').html()),
  id: 'xml-modal-view',
  className: 'xml-modal-view',
  events: {
    'click .modal__wrapper': 'navigateToHide',
    'click .modal__inner': function(e) {
      e.stopPropagation();
    },
    'submit form': 'submit'
  },
  initialize: function() {
    this.render();
    vent.on('XMLModalShow', this.show, this);
    vent.on('XMLModalHide', this.hide, this);
    vent.on('XMLLoadedSuccess', this.successLoaded, this);
    vent.on('XMLLoadedError', this.errorLoaded, this);
    this.model.on('sync', this.stopWait, this);
  },
  show: function() {
    this.$el.addClass('active');
  },
  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    this.$el.find('.modal__submit').addClass('wave');
    this.$el.find('.modal__submit').on('click', makeWave);
    $('#main').append(this.el);
  },
  hide: function() {
    this.$el.removeClass('active');
  },
  navigateToHide: function() {
    this.hide();
    App.router.navigate('#xml-hide');
  },
  doNothing: function(e){
    e.preventDefault();
  },
  submit: function(e) {
    e.preventDefault();
    if(!this.model.set('file', this.$el.find('.modal__input')[0].files[0], {validate: true})) return;
    var self = this;
    this.$el.undelegate('form', 'submit');
    this.$el.find('.modal__input').attr('disabled', 'disabled');
    this.$el.find('.modal__submit').off('click', makeWave);
    this.$el.delegate('form', 'submit', this.doNothing);
    this.$el.find('.modal__submit').addClass('wait');
    this.model.save({
      success: function(response){
        if (response.status==='ok') {
          log(response.message, 'good');
        } else {
          log(response.message, 'bad')
        }
      },
      error: function(response){
        if (response.message) {
          log(response.message, 'bad');
        } else {
          log("Что-то поломалось или априори не работало");
        }
      },
      always: function(){
        self.$el.find('.modal__submit').removeClass('wait');
        self.$el.find('.modal__input').removeAttr('disabled');
        self.$el.find('.modal__submit').on('click', makeWave);
        self.$el.undelegate('form', 'submit');
        self.$el.delegate('form', 'submit', self.submit.bind(self));
      }
    });
  },
  stopWait: function() {
    this.$el.find('input').removeAttr('disabled');
  },
  successLoaded: function() {
    if (this.$el.find('input').next('span').length == 0) {
      this.$el.find('input').after('<span class="modal__message-success">Загрузка выполнена успешно</span>');
    } else {
      this.$el.find('input').next('span').attr('class', '');
      this.$el.find('input').next('span').addClass('modal__message-success').html('Загрузка выполнена успешно');
    }
    this.$el.find('.modal__submit').removeClass('wait');
  },
  errorLoaded: function() {
    if (this.$el.find('input').next('span').length == 0) {
      this.$el.find('input').after('<span class="modal__message-error">Загрузка не удалась</span>');
    } else {
      this.$el.find('input').next('span').attr('class', '');
      this.$el.find('input').next('span').addClass('modal__message-success').html('Загрузка не удалась');
    }
    this.$el.find('.modal__submit').removeClass('wait');
  },
});
//Модель товара
App.Models.Product = Backbone.Model.extend({
  defaults: {
    id: 0,
    name: '',
    price: '',
    article: '',
    measure: '',
    category_id: '',
    currency: '',
    basic_measure: ''
  },
  urlRoot: '/product'
});
App.Models.ProductFull = Backbone.Model.extend({
  defaults: {
    id: 0,
    name: '',
    price: '',
    measure: '',
    category_id: '',
    currency: '',
    basic_measure: '',
    content: '',
  },
  urlRoot: '/productfull',
  initialize: function() {

  }
});
App.Views.Product = Backbone.View.extend({
  model: App.Models.Product,
  tagName: 'tr',
  className: 'product',
  template: _.template($('#product').html()),
  initialize: function() {
    this.render();
  },
  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  },
  events: {
    'click': 'showControls',
    'click .controls': function(e) {
      e.stopPropagation();
    },
    'click .delete': 'deleteModel',
    'click .edit': 'showEdit'
  },
  showControls: function(e) {
    $('.controls.active').removeClass('active');
    $('.product.active').removeClass('active');
    var controls = this.$el.find('.controls');
    this.$el.addClass('active');
    controls.toggleClass('active');
    // console.log('x:'+e.clientX+'   y:'+e.clientY);
    // console.log(e);
    controls.css({
      top: e.pageY + 5 + 'px',
      left: e.pageX - 8 + 'px',
    });
  },
  deleteModel: function() {
    this.model.destroy();
    this.remove();
    log('Товар удален');
  },
  showEdit: function() {
    App.router.navigate('#product/' + this.model.get('id'));
    vent.trigger('showProduct', this.model.get('id'));
  }
});
//Коллекция товаров
App.Collections.Products = Backbone.Collection.extend({
  model: App.Models.Product,
  url: 'product',
  // sortDirection: true,
  comparator: function(model) {
    return model.get("id");
  },
  initialize: function() {
    // console.log('collection initialized');
  },
});
//Вид коллекции
App.Views.Products = Backbone.View.extend({
  tagName: 'table',
  className: 'prod-collection',
  template: _.template($('#products-collection').html()),
  rendered: false,
  htmled: false,
  events: {

  },
  initialize: function() {
    vent.on('indexProducts', this.fetchWithRender, this);
    vent.on('indexCategories', this.remove, this);
    vent.on('showProduct', this.openEdit, this);
  },
  render: function() {
    if (!this.rendered) {
      if (!this.htmled) {
        this.$el.html(this.template())
        this.htmled = true;
      };
      this.renderItems();
      $('#main').prepend(this.el);
      this.rendered = true;
    } else {
      this.$el.css('display', 'table')
    };
    this.$el.off('click', '.prod-collection__head-item');
    this.$el.on('click', '.prod-collection__head-item', this.sortByField.bind(this));
    return this;
  },
  append: function(product) {
    var productView = new App.Views.Product({
      model: product
    });
    this.$el.find('tbody').append(productView.render().el);
  },
  fetchWithRender: function() {
    var self = this;
    this.rendered = false;
    this.collection.fetch({
      success: function() {
        self.render();
      }
    });
  },
  removeAndUnrend: function() {
    this.$el.find('tbody').html('');
    this.rendered = false;
    return this;
  },
  renderItems: function(){
    this.$el.find('tbody').html('');
    this.collection.each(this.append, this);
    this.rendered = true;
  },
  sortByField: function(e){
    var target = e.target;
    var dataSort = $(target).attr('data-sort');
    var dataDirection = $(target).attr('data-direction');
    if (!dataSort) {
      dataSort = $(target).parent().attr('data-sort');
      dataDirection = $(target).parent().attr('data-direction');
      target = target.parentNode;
      if (!dataSort) return;
    }
    if(dataDirection === undefined) {
      dataDirection = true;
      $(target).attr('data-direction', 'false');
    } else {
      dataDirection = JSON.parse(dataDirection);
    }
    if (dataDirection === true) {
      this.collection.models = this.collection.sortBy(dataSort);
      $(target).attr('data-direction', 'false');
      $(target).find('span').removeClass('fa-sort-asc').addClass('fa-sort-desc');
    } else {
      this.collection.models = this.collection.sortBy(dataSort).reverse();
      $(target).attr('data-direction', 'true');
      $(target).find('span').removeClass('fa-sort-desc').addClass('fa-sort-asc');
    }
    this.renderItems();
  },
  openEdit: function(id) {
    var self = this;
    product = _.findWhere(this.collection.models, {
      id: id
    });
    if (!product) {
      product = new App.Models.Product({
        id: id
      });
      product.fetch({
        success: function() {
          self.showEdit(product)
        },
        error: function() {
          log('Такого товара не существует')
        }
      });
    } else {
      self.showEdit(product);
    }
  },
  showEdit: function(product) {
    var id = product.get('id');
    var fullProduct = new App.Models.ProductFull({
      id: id
    })
    var fullView = new App.Views.ProductFull({
      model: fullProduct
    });
  }
});
//Расширенный вид продукта
App.Views.ProductFull = Backbone.View.extend({
  model: App.Models.ProductFull,
  className: 'product-full',
  tagName: 'div',
  template: _.template($('#product-full').html()),
  events: {
    'click': 'removeAndGoToProducts',
    'click .close': 'removeAndGoToProducts',
    'click .product-full__inner': function(e){
      e.stopPropagation();
    },
  },
  initialize: function() {
    var self = this;
    this.model.fetch({
      success: function() {
        self.render();
      }
    });
  },
  render: function(){
    console.log(this.model.attributes);
    var options = {
      product: this.model.toJSON(),
      measures: JSON.parse(App.Storage.getItem('measures')),
      currencies: JSON.parse(App.Storage.getItem('currencies')),
      categories: JSON.parse(App.Storage.getItem('categories')),
    };
    this.$el.html(this.template(options));
    $('#main').find('.product-full').remove();
    $('#main').append(this.el);
  },
  removeAndGoToProducts: function(){
    this.remove();
    App.router.navigate('#product');
    vent.trigger('indexProducts');
  }
})
  //Модель категории
App.Models.Category = Backbone.Model.extend({
  defaults: {
    id: ''
  }
});
//Вид категории
App.Views.Category = Backbone.View.extend({
  model: App.Models.Category,
  tagName: 'tr',
  className: 'category',
  template: _.template($('#category').html()),
  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  },
});
//Коллекция категорий
App.Collections.Categories = Backbone.Collection.extend({
  model: App.Models.Category,
  url: 'category',
  initialize: function() {

  }
});
//Вид коллекции категорий
App.Views.Categories = Backbone.View.extend({
  collection: App.Collections.Categories,
  tagName: 'table',
  rendered: false,
  initialize: function() {
    vent.on('indexCategories', this.fetchWithRender, this);
    vent.on('indexProducts', this.removeAndUnrend, this);
  },
  render: function() {
    if (!this.rendered) {
      this.$el.html('');
      this.collection.each(this.append, this);
      $('#main').prepend(this.el);
      this.rendered = true;
    }
  },
  append: function(category) {
    var categoryView = new App.Views.Category({
      model: category
    });
    this.$el.append(categoryView.render().el);
  },
  fetchWithRender: function() {
    var self = this;
    this.collection.fetch({
      success: function() {
        self.render();
      }
    });
  },
  removeAndUnrend: function() {
    this.remove();
    this.rendered = false;
  }
});
App.Models.User = Backbone.Model.extend({
  urlRoot: '/user',
  defaults: {
    id:'1',
    email: '',
    password: '',
  },
  initialize: function(){
    this.on('invalid', this.showError);
  },
  validate: function(attrs) {
    var self = this;
    if (!validateEmail(attrs.email)) return 'Неверный email';
    if (attrs.newPassword==='') return 'Введите новый пароль';
    if (attrs.newPassword.length<6) return 'Слишком короткий пароль';
    if (attrs.newPassword!==attrs.newPasswordConfirm) return 'Пароли не совпадают';
  },
  showError: function(model, error) {
    log(error);
  }
});
App.Views.UserSettings = Backbone.View.extend({
  className: 'user-settings',
  id: 'user-settings',
  model: App.Models.User,
  template: _.template($('#user-settings').html()),
  events: {
    'click .modal__inner': function(e){
      e.stopPropagation();
    },
    'click .modal__close, .modal__wrapper': function(){
      App.router.navigate('hide-user-settings');
      this.hide();
    },
    'submit form': 'saveSettings',
  },
  initialize: function(){
    this.render();
    vent.on('showUserSettings', this.show, this);
    vent.on('hideUserSettings', this.hide, this);
  },
  render: function(){
    var self = this;
    this.model.fetch({
      success: function(){
        self.$el.html(self.template(self.model.toJSON()));
        $('#main').append(self.el);
      },
      error: function(){
        log("Что-то пошло не так...", 'bad');
      }
    });
  },
  show: function(){
    this.$el.addClass('active');
  },
  hide: function(){
    this.$el.removeClass('active');
  },
  saveSettings: function(e){
    e.preventDefault();
    var el = this.$el;
    var self = this;
    var options = {
      email: el.find('[name="email"]').val(),
      newPassword: el.find('[name="new_password"]').val(),
      newPasswordConfirm: el.find('[name="new_password_confirm"]').val(),
    };
    if (!this.model.set(options, {validate: true})) return;
    var saved = this.model.save();
    if (!saved) {
      log("Что-то поломалось или априори не работало");
      return;
    };
    saved.done(function(response){
      console.log(response);
      log(response.message, 'good');
      self.clear();
    });
  },
  clear: function(){
    this.$el.find('[name="new_password"], [name="new_password_confirm"]').val('');
  }
});
var xmlModal = new App.Models.XMLModal()
var xmlView = new App.Views.XMLModal({
  model: xmlModal
});
var products = new App.Collections.Products();
var productsView = new App.Views.Products({
  collection: products
});
var categories = new App.Collections.Categories();
var categoriesView = new App.Views.Categories({
  collection: categories
});
var user = new App.Models.User();
var userSettings = new App.Views.UserSettings({model: user});
App.router = new App.Router;
Backbone.history.start();
