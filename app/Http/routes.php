<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('index');
// });
// Route::post('/xml', 'XMLController@parse');
// Route::get('product', 'ProductsController@index');
// Route::delete('product/{id}', 'ProductsController@destroy');
// Route::get('category', 'CategoriesController@index');
// Route::get('product/{id}', 'ProductsController@show');
// Route::get('productfull/{id}', 'ProductsController@showFull');
// Route::get('getconfig', 'ConfigsController@index');

Route::get('/', 'PublicController@showPrice');
Route::get('/scriptdata.js', 'PublicController@showDataJson');


Route::get('auth/login', 'Auth\AuthController@loginPage');
Route::post('auth/login', 'Auth\AuthController@login');

Route::get('auth/logout', 'Auth\AuthController@logout');
Route::get('cats', function(){
  $categories = Evth\Models\Category::all();
  foreach ($categories as $category) {
    $category->childs;
  }
  //  Response::json([$category, $childes], 200, ['Content-type'=>'text/html'],JSON_UNESCAPED_UNICODE);
  return  Response::json($categories, 200, ['Content-type'=>'text/html'],JSON_UNESCAPED_UNICODE);
});
Route::group(['middleware'=>'auth'], function(){
  Route::get('/admin', function(){
    return view('index');
  });
  Route::post('/xml', 'XMLController@parse');
  Route::get('product', 'ProductsController@index');
  Route::get('product/{id}', 'ProductsController@show');
  Route::delete('product/{id}', 'ProductsController@destroy');

  Route::get('productfull/{id}', 'ProductsController@showFull');
  Route::get('category', 'CategoriesController@index');
  Route::get('getconfig', 'ConfigsController@index');

  Route::get('user/{id}', 'UsersController@show');
  Route::put('user/{id}', 'UsersController@update');
  Route::post('checkpassword', 'UsersController@checkPassword');
});
// Route::get('/admin', function () {
//     return view('index');
// });
