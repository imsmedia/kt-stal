var date = new Date();
var currentDateString = [];
currentDateString.push(date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
currentDateString.push(parseInt(date.getMonth())+1 < 10 ? '0' + (parseInt(date.getMonth())+1) : (parseInt(date.getMonth())+1));
currentDateString.push(date.getFullYear());
$('#current-date').html(currentDateString.join('.'));

var App = {
  Models: {},
  Collections: {},
  Views: {},
  Data: JSON.parse($('#server-data').html())
}

App.Views.Cat = Backbone.View.extend({
  tagName: 'li',
  events: {
    'click' : 'show',
  },
  initialize: function(){
    this.$el.html(this.model.get('name'));
    return this;
  },
  show: function(){
    this.showProducts();
    this.showName();
    $('.sidebar .active').removeAttr('class');
    this.$el.addClass('active');
  },
  showProducts: function(){
    var self = this;
    var products = _.filter(App.Data.products, function(product){
      return product.category_id === self.model.get('id')
    });
    var prodsCol = new Backbone.Collection(products);
    var prodsView = new App.Views.Products({collection: prodsCol});
    prodsView.render();
  },
  showName: function(){
    $('#cat-name').html(this.model.get('name'));
  }
});
App.Views.Cats = Backbone.View.extend({
  collection: cats,
  tagName: 'ul',
  id: 'cats-list',
  initialize: function(){
    this.collection.models = this.collection.sortBy('name');
  },
  render: function(){
    this.collection.each(function(model) {
      var view = new App.Views.Cat({model: model});
      this.appendItem(view);
    }, this);
    $('aside').append(this.el);
  },
  appendItem: function(view){
    this.$el.append(view.el)
  },

});
App.Views.Product = Backbone.View.extend({
  tagName: 'tr',
  template: _.template('<td><%= name %></td><td><%= price ? price.toFixed(2) : "Договорная" %></td>'),
  initialize: function(){
    this.render();
  },
  render: function(){
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  }
});
App.Views.Products = Backbone.View.extend({
  tagName: 'tbody',
  initialize: function(){

  },
  render: function(){
    this.collection.each(function(model) {
      var view = new App.Views.Product({model: model});
      this.appendItem(view);
    }, this);
    $('.products-table tbody').html('');
    $('.products-table').append(this.el);
  },
  appendItem: function(view){
    this.$el.append(view.el)
  },
});
var cats = new Backbone.Collection(App.Data.categories);
var catView = new App.Views.Cats({collection: cats});
catView.render();
$('#cats-list li')[0].click();
