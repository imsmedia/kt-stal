Backbone.emulateHTTP = true;
//global App object for app
var App = {
  Models: {},
  Views: {},
  Collections: {},
  Storage: window.localStorage,
};
//events object for trigger app logic
var vent = _.extend({}, Backbone.Events);
App.getConfig = function(){
  var option = this.option ? this.option : 'normal';
  var live = App.Storage.getItem('live') ? App.Storage.getItem('live') : 1;
  if (live===1 || (new Date(+live)).valueOf()<(new Date()).valueOf() || option==='force') {
    $.ajax({
      url: '/getconfig',
      type: 'GET',
      dataType: 'text',
    })
    .done(function(response) {
      var result = JSON.parse(response);
      App.Storage.setItem('measures', JSON.stringify(result.measures));
      App.Storage.setItem('currencies', JSON.stringify(result.currencies));
      App.Storage.setItem('categories', JSON.stringify(result.categories));
      App.Storage.setItem('live', (new Date()).valueOf()+60*60*1000);
    })
    .fail(function() {
      console.log('fail config loading from "/getconfig" url');
    });
  }
}
vent.on('getConfig',App.getConfig);
vent.trigger('getConfig');
$('#refresh-config').on('click', App.getConfig.bind({option: 'force'}));
