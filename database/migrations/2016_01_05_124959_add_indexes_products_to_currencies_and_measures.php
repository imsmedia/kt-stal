<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesProductsToCurrenciesAndMeasures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('currency')->references('id')->on('currencies')->onUpdate('cascade');
            $table->foreign('measure')->references('id')->on('measures')->onUpdate('cascade');
            $table->foreign('basic_measure')->references('id')->on('measures')->onUpdate('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_measure_foreign');
            $table->dropForeign('products_basic_measure_foreign');
            $table->dropForeign('products_currency_foreign');           
        });
    }
}
