<?php
return [
  'bad_loading'=>'{"status":"error","message":"Загрузка не удалась"}',
  'bad_file_type'=>'{"status":"error","message":"Неверный тип файла"}',
  'success_loading'=>'{"status":"ok","message":"Загрузка успешно завершена"}',
  'password_check_success'=>'{"status":"ok","message":"Пароль соответствует старому"}',
  'password_check_error'=>'{"status":"error","message":"Неверный пароль"}',
  'user_update_success'=>'{"status":"ok","message":"Пользователь успешно обновлен"}'
];
