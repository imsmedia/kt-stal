App.Router = Backbone.Router.extend({
  routes: {
    '': 'start',
    'xml-show': 'showXMLModal',
    'xml-hide': 'hideXMLModal',
    'product': 'indexProducts',
    'product/:id': 'showProduct',
    'category': 'indexCategories',
    'user-settings': 'showUserSettings',
    'hide-user-settings': 'hideUserSettings',
  },
  start: function() {
    console.log('Стартовая страница');
    vent.trigger('XMLModalHide');
  },
  showXMLModal: function() {
    vent.trigger('XMLModalShow');
  },
  hideXMLModal: function() {
    vent.trigger('XMLModalHide');
  },
  indexProducts: function(){
    $('.product-full').remove();
    vent.trigger('indexProducts');
  },
  indexCategories: function(){
    vent.trigger('indexCategories');
    console.log('categories route');
  },
  showProduct: function(id){
    vent.trigger('showProduct', id);
  },
  showUserSettings: function(){    
    vent.trigger('showUserSettings');
  },
  hideUserSettings: function(){
    vent.trigger('hideUserSettings');
  }
});
