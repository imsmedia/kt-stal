function makeWave(e){
  if ($(this).parent().find('.ink').length===0) {
    var wave = document.createElement('i');
    wave = $(wave);
    wave.addClass('ink');
    $(this).parent().append(wave);
  } else {
    wave = $(this).parent().find('.ink');
  }
  wave.removeClass('animated');
  $(this).append(wave);
  var d = ($(this).outerWidth() >= $(this).outerHeight()) ? $(this).outerWidth() : $(this).outerHeight();
  $(wave).css({
    width: d+'px',
    height: d+'px',
    top: (e.pageY-$(this).offset().top-d/2)+'px',
    left: (e.pageX-$(this).offset().left-d/2)+'px'
  });
  wave.addClass('animated');
};
$(function() {
  var waves = $('.wave');
  var sidebarLinks = $('.sidebar__link');
  waves.on('click', makeWave);
  sidebarLinks.on('click', function(){
    sidebarLinks.removeClass('active');
    $(this).addClass('active');
  })
});
function log(error, kind) {
  $('#errors').html(error);
  if (kind===undefined) {
    $('#errors').attr('class', 'active bad');
    setTimeout(function() {
      $('#errors').removeClass('active');
    }, 3000);
  } else {
    $('#errors').attr('class', 'active '+kind);
    setTimeout(function() {
      $('#errors').removeClass('active');
    }, 3000);
  }
};
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
