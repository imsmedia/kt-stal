<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <title>Прайс на продукцию kt-stal.com.ua</title>
  <meta name="viewport" content="width=1200, initial-scale=1">
  <link rel="stylesheet" href="/evth/css/price.css">
  <script src="/evth/js/libs/jquery.js"></script>
  <script src="/evth/js/libs/underscore.js"></script>
  <script src="/evth/js/libs/backbone.js"></script>
</head>

<body>
  <div class="overlay">
    <div class="header-wrapper">
      <header class="header">
        <div class="header__left">
          <p class="header__paragraph">г. Харьков, ул. Киргизская 19, въезд на территорию ХЗТД</p>
          <p class="header__paragraph">т.: (067) 577 33 39, (095) 011-18-80, (057) 780-45-59</p>
        </div>
        <div class="header__right">
          <p class="header__paragraph">наличный расчет, безналичный расчет</p>
          <p class="header__paragraph"><a class="header__link" target="_blank" href="http://kt-stal.com.ua">kt-stal.com.ua</a>, <a target="_blank" class="header__link" href="http://metall-baza.com">metall-baza.com</a>, <a class="header__link" href="mailto:kt-stal@ukr.net">kt-stal@ukr.net</a></p>
        </div>
      </header>
    </div>
    <!-- <div class="title-bg"></div> -->
    <div class="wrapper">
      <div class="inner">
        <aside class="sidebar">
          <div class="sidebar__logo">
            <a href="/" class="sidebar__logo-link">
              <img src="/evth/img/price/logo.png" alt="" class="sidebar__logo-img">
            </a>
          </div>
        </aside>
        <div class="content">
          <div class="content-header">
            <span class="content-header__title" id="cat-name">Доставка по Украине фура 22т тент</span>
            <span class="content-header__description">Прайс на продукцию КТ-СТАЛЬ от <span id="current-date"></span></span>
          </div>
          <div class="specials">
            <table class="specials__table">
              <tbody>
                <tr>
                  <td>При покупке арматуры, сетки, уголка, полосы, квадрата, круга на сумму:</td>
                  <td class="specials__condition">более 5000грн</td>
                  <td class="specials__condition">более 10000грн</td>
                  <td class="specials__condition">более 20000грн</td>
                  <td class="specials__condition">более 40000грн</td>
                  <td class="specials__condition">более 80000грн</td>
                </tr>
                <tr>
                  <td> Вы гарантированно получаете скидку:</td>
                  <td class="specials__result">0,5%</td>
                  <td class="specials__result">1%</td>
                  <td class="specials__result">1,5%</td>
                  <td class="specials__result">2%</td>
                  <td class="specials__result">2,5%</td>
                </tr>
              </tbody>
            </table>
            <span class="specials__propose">
              БЕСПЛАТНАЯ доставка по городу при заказе на сумму свыше 19 999 тыс.грн.
            </span>
            <table class="products-table">
              <thead>
                <tr>
                  <th>Наименование</th>
                  <th>Цена, грн</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="footer__inner">
      <p class="footer__copywrite">
        2015 © — Компания «КТ-Сталь». Все права соблюдены.
      </p>
      <p class="footer__copywrite">
        База металлопроката, выгодные цены на строительную арматуру, профильные трубы, сварную сетку и другое. Купить по цене производителя с доставкой по Украине.
      </p>
    </div>
  </footer>
  <script type="text/template" id="server-data">
    <?php print(json_encode($arr)); ?>
  </script>
  <script type="text/template" id="category">
    <%= name %>
    <ul>
      <%  _.each(childs, function(el, index){ %>
        <li><%= el.name %></li>
      <% }, childs); %>
    </ul>
  </script>
  <!-- <script src="/evth/js/price.js"></script> -->
  <script src="/evth/js/price_multicats.js"></script>
</body>

</html>
