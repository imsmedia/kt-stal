<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Страница входа</title>
  <link rel="stylesheet" href="/evth/css/auth.css">
</head>

<body>
  <div class="login-form">
    <span class="login-form__title">Войти на сайт</span>
    @foreach ($errors->all() as $error)
      <span class="login-form__error">{{ $error }}</span>
    @endforeach
    <form action="/auth/login" method="post">
      <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}" />
      <div class="login-form__input-wrapper">
        <label class="login-form__label" for="input1">
          Email
        </label>
        <input class="login-form__input" type="text" name="email" id="input1" value="{{Session::get('_old_input.email')}}">
      </div>
      <div class="login-form__input-wrapper">
        <label class="login-form__label" for="input2">
          Пароль
        </label>
        <input class="login-form__input" type="password" name="password" id="input2">
      </div>
      <div class="login-form__input-wrapper">
        <button class="login-form__button" type="submit">Войти</button>
      </div>
    </form>
  </div>
</body>

</html>
