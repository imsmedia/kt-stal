<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToCatToCatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_to_cat', function (Blueprint $table) {
            $table->foreign('child_id')->references('id')->on('categories');
            $table->foreign('parent_id')->references('id')->on('categories');
            $table->unique(['child_id','parent_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_to_cat', function (Blueprint $table) {            
            $table->dropForeign('cat_to_cat_child_id_foreign');
            $table->dropForeign('cat_to_cat_parent_id_foreign');
            $table->dropUnique('cat_to_cat_child_id_parent_id_unique');
        });
    }
}
