<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use SimpleXMLElement;
use Evth\Models\Category;
use Evth\Models\Product;
use Evth\Models\Currency;
use Evth\Models\Measure;
use DB;
class XMLController extends Controller
{
  public function __construct(Request $request) {
        $this->request = $request;
    }
  public function parse() {
    //получаем и перемещаем файл
    $file = $this->request->file('file');
    $destinationPath = public_path();
    if (!$file->isValid()) return abort(404, trans('json.bad_loading'));//Проверка корректной загрузки файла
    $validator = Validator::make(['file'=>$file],['file' => 'mimes:application,xml']);//Правила проверки на соответсвтие mime-типу
    if ($validator->fails()) return abort(404, trans('json.bad_file_type'));
    $newFileName = str_random(10).'.'.$file->guessExtension();
    $file->move($destinationPath, $newFileName);

    //парсим файл
    $str = file_get_contents(asset($newFileName));
    $str = str_replace('windows-1251', 'utf-8', $str);
    $str = mb_convert_encoding($str, 'utf-8', 'windows-1251');
    $str = str_replace('Валюта', 'currency', $str);
    $str = str_replace('БазоваяЕдиницаИзмерения', 'basic_measure', $str);
    $str = str_replace('ЕдиницаИзмерения', 'measure', $str);
    $xml = new SimpleXMLElement($str);
    $products = $xml->product;
    $currencies = [];
    $measures = [];
    foreach ($products as $product) {
      $currencies[] = $product['currency'];
      $measures[] = $product['measure'];
    }
    $currencies = array_unique($currencies);
    $measures = array_unique($measures);
    foreach ($currencies as $currency) {
      $name = $currency->__toString();
      $data = ['name'=>$name];
      $dbCurrency = new Currency($data);
      if ($dbCurrency->isValid($data)) {
        $dbCurrency->save();
      }
      unset($data);
    }
    foreach ($measures as $measure) {
      $name = $measure->__toString();
      $data = ['name'=>$name];
      $dbMeasure = new Measure($data);
      if ($dbMeasure->isValid($data)) {
        $dbMeasure->save();
      }
      unset($data);
    }
    $categories = [];
    $categoryIds = DB::table('categories')->lists('id');
    foreach ($products as $product) {
      if ($product['ЭтоГруппа']=='истина') {
        if ( !in_array(intval($product['Код']), $categoryIds) ) {
          $data = [
            'id'=>$product['Код'],
            'name'=>$product['Наименование']
          ];
          $dbCategory = new Category($data);
        } else {
          $dbCategory = Category::find(intval($product['Код']));
          $dbCategory->name = $product['Наименование'];
        }
        $dbCategory->save();
        $categories[]=$product;
      }
    }
    DB::table('cat_to_cat')->delete();
    foreach ($categories as $category) {
      if ($category['КодКатегории']!='') {
        try {
          DB::table('cat_to_cat')->insert([
            'parent_id'=>$category['КодКатегории'],
            'child_id'=>$category['Код']
          ]);
        } catch (Exception $e) {
          Log::info($e->getMessage());
        }
      }
    }
    $measures = Measure::all()->toArray();
    for ($i=0; $i < count($measures); $i++) {
      $measuresArray[$measures[$i]['name']] = $measures[$i]['id'];
    }
    $currencies = Currency::all()->toArray();
    for ($i=0; $i < count($currencies); $i++) {
      $currenciesArray[$currencies[$i]['name']] = $currencies[$i]['id'];
    }
    $productIds = DB::table('products')->lists('id');
    foreach ($products as $product) {
      if ($product['ЭтоГруппа']=='ложь') {
        $currency = $product['currency']->__toString() ? $currenciesArray[$product['currency']->__toString()] : null;
        $measure = $product['measure']->__toString() ? $measuresArray[$product['measure']->__toString()] : null;
        $basic_measure = $product['basic_measure']->__toString() ? $measuresArray[$product['basic_measure']->__toString()] : null;
        $category_id = $product['КодКатегории']->__toString() ? $product['КодКатегории']->__toString() : null;
        if ( !in_array( intval($product['Код']), $productIds ) ) {
          $dbProduct = new Product([
            'id'=>$product['Код'],
            'article'=>$product['Артикул'],
            'name'=>$product['Наименование'],
            'price'=>str_replace(',', '.', $product['Цена']),
            'currency'=>$currency,
            'measure'=>$measure,
            'basic_measure'=>$basic_measure,
            'category_id'=>$category_id
          ]);
          $dbProduct->save();
        } else {
          $dbProduct = Product::find(intval($product['Код']));
          $dbProduct->article = $product['Артикул'];
          $dbProduct->name = $product['Наименование'];
          $dbProduct->price = str_replace(',', '.', $product['Цена']);
          $dbProduct->currency = $currency;
          $dbProduct->measure = $measure;
          $dbProduct->basic_measure = $basic_measure;
          $dbProduct->category_id = $category_id;
          $dbProduct->save();
        }
      }
    }
    return trans('json.success_loading');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
