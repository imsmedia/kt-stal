<?php
namespace Evth\Models;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
class Content extends Model {
  public $timestamps = false;
  protected $fillable = ['content','product_id'];  
}
