<?php
/*Таблица товаров*/
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article')->unsigned();
            $table->string('name');
            $table->float('price');
            $table->integer('currency')->unsigned()->nullable();
            $table->integer('measure')->unsigned()->nullable();
            $table->integer('basic_measure')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
