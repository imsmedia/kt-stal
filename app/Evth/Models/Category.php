<?php
namespace Evth\Models;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use Validator;
class Category extends Model {
  public $timestamps = false;
  protected $fillable = ['id', 'name'];
  public static $errors;
  public static $rules = [];
  public function isValid($data){
    $validator = Validator::make($data, static::$rules);
    if ($validator->passes()) return true;
    static::$errors = $validator->messages();
    return false;
  }
  public function childs(){
    return $this->belongsToMany('Evth\Models\Category', 'cat_to_cat', 'parent_id', 'child_id');
  }
  public function products(){
    return $this->hasMany('Evth\Models\Product');
  }  
}
