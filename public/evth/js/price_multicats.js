var App = {
  Models: {},
  Collections: {},
  Views: {},
  Data: JSON.parse($('#server-data').html())
};
var date = new Date();
var currentDateString = [];
currentDateString.push(date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
currentDateString.push(parseInt(date.getMonth())+1 < 10 ? '0' + (parseInt(date.getMonth())+1) : (parseInt(date.getMonth())+1));
currentDateString.push(date.getFullYear());
$('#current-date').html(currentDateString.join('.'));

function getObj(cats){
  var childIds = {};
  var parentIds = {};
  var catsObj = {};
  for (var i = 0; i < cats.length; i++) {
    var cat = cats[i];
    catsObj[cat.id] = {name: cat.name};
    if(cat.childs.length > 0) {
      catsObj[cat.id].childs = {};
      for (var j = 0; j < cat.childs.length; j++) {
        catsObj[cat.id].childs[cat.childs[j].id] = cat.childs[j].id;
      };
    }
  };
  for (var i = 0; i < cats.length; i++) {
    var cat = cats[i];
    if(cat.childs.length > 0) {
      for (var j = 0; j < cat.childs.length; j++) {
        childIds[cat.childs[j].pivot.child_id] = cat.childs[j].pivot.child_id;
        parentIds[cat.childs[j].pivot.parent_id] = cat.childs[j].pivot.parent_id;
      };
    };
  };
  for (var key in catsObj) {
    if (catsObj.hasOwnProperty(key)) {

    }
  };
  return catsObj;
};
var catsObj = getObj(App.Data.categories);
var catsObj = _.extend({}, catsObj);
function recurReader(obj){//элемент объекта
  if (obj && obj.hasOwnProperty('childs')) {
    var childs = obj.childs;
    for (var key in childs) {
      childs[key] = catsObj[key];
      recurReader(childs[key]);
      delete catsObj[key];
    }
  }
}
var keys = Object.keys(catsObj);
recurReader(catsObj[2]);
// console.log(JSON.stringify(catsObj));

var createEl = document.createElement.bind(document);
var Cat = Backbone.View.extend({
  tagName: 'li',
  events: {
    'click' : 'show',
  },
  show: function(){
    this.showProducts();
    this.showName();
    $('.sidebar li.active').removeAttr('class');
    this.$el.addClass('active');
  },
  showProducts: function(){
    var self = this;
    var products = _.filter(App.Data.products, function(product){
      return product.category_id === self.model.get('id')
    });
    var prodsCol = new Backbone.Collection(products);
    var prodsView = new App.Views.Products({collection: prodsCol});
    prodsView.render();
  },
  showName: function(){
    $('#cat-name').html(this.model.get('name'));
  },
  render: function(){
    this.$el.html(this.model.get('name'));
    return this;
  }
});
var Cats = Backbone.View.extend({
  collection: new Backbone.Collection((new Backbone.Collection(App.Data.categories)).sortBy('name')),
  vents: _.extend({}, Backbone.Events),
  initialize: function(){
    this.vents.on('render', function(){

    }, this);
  },
  events: {
    'click .has-child': function(e){
      this.$el.find('p.opened').removeClass('opened');
      this.$el.find('.active').removeClass('active');
      $(e.target).next('ul').toggleClass('active');
      $(e.target).addClass('opened');
      var self = this;
      var products = _.filter(App.Data.products, function(product){
        return product.category_id === +$(e.target).attr('data-id');
      });
      if (products.length>0) {
        var prodsCol = new Backbone.Collection(products);
        var prodsView = new App.Views.Products({collection: prodsCol});
        prodsView.render();
        $(e.target).addClass('active');
      };
    },
  },
  render: function () {
    function recur(childs){
      for (var i = 0; i < childs.length; i++) {
        if (childs[i]) {

        }
        console.log(childs[i]);
      }
    };
    this.collection.each(function(model, index) {
      var div = createEl('div');
      var p = createEl('p');
      $(p).html(model.get('name'));
      $(div).append(p);
      var childs = model.get('childs');
      if (childs.length > 0) {
        $(p).addClass('has-child');
        $(p).attr('data-id', model.get('id'));
        var ul = createEl('ul');
        for (var i = 0; i < childs.length; i++) {
          var view = new Cat({model: new Backbone.Model(childs[i])});
          $(ul).append(view.render().el);
        }
        $(div).append(ul);
        $(this).append(div);

      } else {
        $(p).addClass('no-child');
      };
    }, this.el);
    $('.sidebar').append(this.el);
    this.vents.trigger('render');
  }
});
App.Views.Product = Backbone.View.extend({
  tagName: 'tr',
  template: _.template('<td><%= name %></td><td><%= price ? price.toFixed(2) : "Договорная" %></td>'),
  initialize: function(){
    this.render();
  },
  render: function(){
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  }
});
App.Views.Products = Backbone.View.extend({
  tagName: 'tbody',
  initialize: function(){

  },
  render: function(){
    this.collection.each(function(model) {
      var view = new App.Views.Product({model: model});
      this.appendItem(view);
    }, this);
    $('.products-table tbody').html('');
    $('.products-table').append(this.el);
  },
  appendItem: function(view){
    this.$el.append(view.el)
  },
});
var cats = new Cats();
cats.render();

var div = createEl('div');
var level = 0;
var str ='';
function recurWriter(obj){
  var keys = Object.keys(obj);
  var firstKey = keys[0];
  var lastKey = keys[keys.length-1];
  for (var key in obj) {
    if (key === firstKey) {
      str += '<ul>';
    };
    if (obj[key].hasOwnProperty('childs')) {
      str += '<li class="with-childs" data-id="'+key+'" data-name="'+obj[key].name+'">' + obj[key].name + '</li>';
      recurWriter(obj[key].childs);
    } else {
      str += '<li class="without-childs" data-id="'+key+'" data-name="'+obj[key].name+'">'+obj[key].name+'</li>';
    };
    if (key === lastKey) {
      str += '</ul>';
    };
  };
};
recurWriter(catsObj);
$('.sidebar').html(str);
$('.sidebar .with-childs').on('click', function(e) {

});
$('.sidebar .without-childs').on('click', function(e) {
  $('.sidebar .without-childs.active').removeClass('active');
  // $(this).addClass('active');
});
$('.sidebar li').on('click', function(e) {
  if ($(this).hasClass('active') && $(this).hasClass('with-childs')) {
    $(this).removeClass('active');
    return ;
  }
  $(this).addClass('active');
  $('#cat-name').html($(this).attr('data-name'));
  var products = _.filter(App.Data.products, function(product){
    return product.category_id === +$(e.target).attr('data-id');
  });
  if (products.length===0) {
    products.push({name: 'Нет товаров', price: 0});
  } else {
    $(this).addClass('with-products');
  };
  var prodsCol = new Backbone.Collection(products);
  var prodsView = new App.Views.Products({collection: prodsCol});
  prodsView.render();
});
// var newObj = _.extend({}, catsObj);
// function func(obj){//элемент объекта
//   if (obj && obj.hasOwnProperty('childs')) {
//     var childs = obj.childs;
//     for (var key in childs) {
//       childs[key] = newObj[key];
//       func(childs[key]);
//       delete newObj[key];
//     }
//   }
// }
// var keys = Object.keys(newObj);
// func(newObj[2]);
// console.log(newObj);
