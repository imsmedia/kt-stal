<?php
namespace Evth\Models;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
class Product extends Model {
  public $timestamps = false;
  protected $fillable = ['id','article','name','price','currency','measure','base_measure','category_id',];
  public function content(){
    return $this->hasOne('Evth\Models\Content');
  }
  public function measure(){
    return $this->belongsTo('Evth\Models\Measure', 'measure');
  }
  public function basicMeasure(){
    return $this->belongsTo('Evth\Models\Measure', 'basic_measure');
  }
  public function category(){
    return $this->belongsTo('Evth\Models\Category');
  }
  public function sayHello() {
    return 'Hello, I\'m Product Model';
  }
}
