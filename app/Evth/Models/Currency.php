<?php
namespace Evth\Models;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use Validator;
class Currency extends Model {
  public $timestamps = false;
  protected $fillable = ['name'];
  public static $errors;
  public static $rules = [
    'name'=>'required|unique:currencies'
  ];
  public function isValid($data){
    $validator = Validator::make($data, static::$rules);
    if ($validator->passes()) return true;
    static::$errors = $validator->messages();
    return false;
  }
}
